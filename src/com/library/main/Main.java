package com.library.main;

import java.io.BufferedReader;
import java.util.*;
import java.util.stream.IntStream;

import static java.lang.System.exit;

public class Main {

    public static void main(String[] args) {
        ArrayList<Library> library = createLibrary();

        System.out.println("Welcome to the Library System\n");

        int option;
        do
        {
            showMenu();
            option = intScanner();
            switch (option) {
                case 1 -> {
                    System.out.println(displayItem(library));
                    System.out.println("Please type a name of the item you would like to borrow: ");
                    String itemName = strScanner();
                    removeItem(itemName, library);
                }
                case 10 -> exit(0);
                default -> System.out.println("Invalid Menu Option");
            }
        } while (option != 10);
    }

    public static void showMenu(){
        String sb = "1. Borrow a Book\n" +
                "10. Exit Library\n";
        System.out.println(sb);
        System.out.println("Please select the menu option desired");
    }

    public static StringBuilder displayItem(ArrayList<Library> library) {
        StringBuilder sb = new StringBuilder();
        sb.append("Title");
        sb.append("\t\t");
        sb.append("Genre");
        sb.append("\n");
        for (Library l:library) {
            sb.append(library.indexOf(l)+1).append(" ");
            sb.append(l.getName());
            sb.append("\t\t");
            sb.append(l.getGenre());
            sb.append("\n");
        }
        return sb;
    }

    public static void removeItem(String name, ArrayList<Library> library) {
        boolean found = false;
        for (Library l:library) {
            if (name.equalsIgnoreCase(l.getName()) && l.isBorrow()) {
                found = true;
                library.remove(l);
                System.out.println(name + " was borrowed");
                break;
            }
        }
        if (!found)
        {
            System.out.println("Item was not found\n");
        }

    }

    public static ArrayList<Library> createLibrary() {
        ArrayList<Library> library = new ArrayList<>();
       Library[] libraryArray = {
                new Book("Bible", true, "Religion"),
                new Book("Harry Potter", true, "Fiction"),
                new Book("Game of Thrones", true, "Fiction"),
                new Book("Quiz Book", true, "Fact"),
                new Book("Gordan Ramsay", true, "Cooking"),
                new Book("LOTR", true, "Fantasy")
        };
        Collections.addAll(library, libraryArray);
        return library;
    }

    public static String strScanner() {
        Scanner sc = new Scanner(System.in);
        String rtnString = sc.nextLine();
        return rtnString;
    }

    public static int intScanner() {
        Scanner sc = new Scanner(System.in);
        int rtnInt = sc.nextInt();
        return rtnInt;
    }
}


