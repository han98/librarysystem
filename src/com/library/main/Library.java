package com.library.main;

public class Library {

    private String name;
    private boolean borrow;
    private String genre;

    public Library(String name, boolean borrow, String genre) {
        this.name = name;
        this.borrow = borrow;
        this.genre = genre;
    }

    public Library(){

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public boolean isBorrow() {
        return borrow;
    }

    public void setBorrow(boolean borrow) {
        this.borrow = borrow;
    }
}
